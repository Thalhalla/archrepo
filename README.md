# archrepo

[Thalhalla Arch Linux Repository](https://thalhalla.gitlab.io/archrepo/)

Add this to your `/etc/pacman.conf` and `pacman -Sy`

```
[archrepo]
SigLevel = Optional TrustedOnly
Server = https://thalhalla.gitlab.io/$repo/$arch
```

And then install any of the packages listed below e.g. `pacman -S brave-bin`
